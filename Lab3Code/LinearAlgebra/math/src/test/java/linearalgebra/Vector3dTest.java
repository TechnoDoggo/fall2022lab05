package linearalgebra;
import static org.junit.Assert.assertEquals;
import java.lang.Math;
import org.junit.Test;
public class Vector3dTest 
{
    @Test
    public void getMethodTest()
    {
    Vector3d TestVector1 = new Vector3d(1,12,4);
        assertEquals(1,TestVector1.getX(),0);
        assertEquals(12,TestVector1.getY(),0);
        assertEquals(4,TestVector1.getZ(),0);
    }

    @Test
    public void magnitudeTest()
    {
        Vector3d TestVector2 = new Vector3d(2.5,12,1);
        assertEquals(Math.sqrt((2.5 * 2.5) + (12 * 12) + (1 * 1)), TestVector2.magnitude(),0);
    }

    @Test
    public void dotProductTest()
    {
        Vector3d TestVector31 = new Vector3d(2,12,2.4);
        Vector3d TestVector32 = new Vector3d(3.1,2,11);
        assertEquals((2 * 3.1) + (12 * 2) + (2.4 * 11), TestVector31.dotProduct(TestVector32), 0);
    }

    @Test
    public void addTest()
    {
        Vector3d TestVector41 = new Vector3d(12,2.3,8);
        Vector3d TestVector42 = new Vector3d(4.3,18,2.1);
        Vector3d ResultsVector = new Vector3d(0,0,0);
        ResultsVector = TestVector41.add(TestVector42);
        assertEquals(16.3,ResultsVector.getX(),0);
        assertEquals(20.3,ResultsVector.getY(),0);
        assertEquals(10.1,ResultsVector.getZ(),0);
    }
}
