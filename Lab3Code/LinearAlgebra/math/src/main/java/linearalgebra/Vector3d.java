package linearalgebra;
import java.lang.Math;

public class Vector3d
{
    private double x;
    private double y;
    private double z;

    public Vector3d(double newX, double newY, double newZ)
    {
        x = newX;
        y = newY;
        z = newZ;
    }

    public double getX()
    {
        return x;
    }
    
    public double getY()
    {
        return y;
    }

    public double getZ()
    {
        return z;
    }

    public double magnitude()
    {
        double magnitude = Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
        return magnitude;
    }

    public double dotProduct(Vector3d VectorToMultiply)
    {
        double returnDouble;
        returnDouble = (this.x * VectorToMultiply.getX()) + (this.y * VectorToMultiply.getY()) + (this.z * VectorToMultiply.getZ());
        return returnDouble;
    }

    public Vector3d add(Vector3d VectorToAdd)
    {
        Vector3d ReturnVector = new Vector3d(0,0,0);
        ReturnVector.x = this.x + VectorToAdd.getX();
        ReturnVector.y = this.y + VectorToAdd.getY();
        ReturnVector.z = this.z + VectorToAdd.getZ();
        return ReturnVector;
    }

}